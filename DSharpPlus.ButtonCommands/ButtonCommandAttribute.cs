﻿using System;

namespace DSharpPlus.ButtonCommands
{
	public class ButtonCommandAttribute : Attribute
	{
		public string Name { get; }

		public ButtonCommandAttribute(string name)
		{
			Name = name;
		}
	}
}