using System.Threading.Tasks;
using DSharpPlus.Entities;

namespace DSharpPlus.ButtonCommands.Converters
{
	public class DiscordUserConverter : IButtonArgumentConverter<DiscordUser>
	{
		public async Task<Optional<DiscordUser>> ConvertAsync(string value, ButtonContext ctx)
		{
			if (!ulong.TryParse(value, out var id)) return Optional.FromNoValue<DiscordUser>();
			
			DiscordUser result = await ctx.Client.GetUserAsync(id);
			return result != null ? Optional.FromValue(result) : Optional.FromNoValue<DiscordUser>();
		}

		public string ConvertToString(DiscordUser value) => value.Id.ToString();
	}
	
	public class DiscordMemberConverter : IButtonArgumentConverter<DiscordMember>
	{
		public async Task<Optional<DiscordMember>> ConvertAsync(string value, ButtonContext ctx)
		{
			if (!ulong.TryParse(value, out var id)) return Optional.FromNoValue<DiscordMember>();
			
			DiscordMember result = await ctx.Guild.GetMemberAsync(id);
			return result != null ? Optional.FromValue(result) : Optional.FromNoValue<DiscordMember>();
		}

		public string ConvertToString(DiscordMember value) => value.Id.ToString();
	}
	
	public class DiscordRoleConverter : IButtonArgumentConverter<DiscordRole>
	{
#pragma warning disable 1998
		public async Task<Optional<DiscordRole>> ConvertAsync(string value, ButtonContext ctx)
#pragma warning restore 1998
		{
			if (!ulong.TryParse(value, out var id)) return Optional.FromNoValue<DiscordRole>();
			
			DiscordRole result = ctx.Guild.GetRole(id);
			return result != null ? Optional.FromValue(result) : Optional.FromNoValue<DiscordRole>();
		}

		public string ConvertToString(DiscordRole value) => value.Id.ToString();
	}
	
	public class DiscordChannelConverter : IButtonArgumentConverter<DiscordChannel>
	{
		public async Task<Optional<DiscordChannel>> ConvertAsync(string value, ButtonContext ctx)
		{
			if (!ulong.TryParse(value, out var id)) return Optional.FromNoValue<DiscordChannel>();
			
			DiscordChannel result = await ctx.Client.GetChannelAsync(id);
			return result != null ? Optional.FromValue(result) : Optional.FromNoValue<DiscordChannel>();
		}

		public string ConvertToString(DiscordChannel value) => value.Id.ToString();
	}
}