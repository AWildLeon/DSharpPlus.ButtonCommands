using DSharpPlus.AsyncEvents;
using DSharpPlus.ButtonCommands.Converters;
using DSharpPlus.ButtonCommands.EventArgs;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
// ReSharper disable PossibleNullReferenceException

namespace DSharpPlus.ButtonCommands
{
    public class ButtonCommandsExtension : BaseExtension
	{
		private bool Disposed { get; set; } = false;

#pragma warning disable CS0108, CS0114 // Will Be Set By The Setup Method
        private DiscordClient Client { get; set; }
#pragma warning restore CS0108, CS0114

        private ButtonCommandsConfiguration Configuration { get; }
		private Dictionary<string, ButtonCommand> Commands { get; }
		private Dictionary<Type, IButtonArgumentConverter> ArgumentConverters { get; }
		private MethodInfo ConvertMethod { get; }
		private MethodInfo ConvertToButtonIdMethod { get; }
		
		public event AsyncEventHandler<ButtonCommandsExtension, ButtonCommandExecutionEventArgs> ButtonCommandExecuted
		{
			add => _executed.Register(value);
			remove => _executed.Unregister(value);
		}
		private AsyncEvent<ButtonCommandsExtension, ButtonCommandExecutionEventArgs> _executed;
		
		public event AsyncEventHandler<ButtonCommandsExtension, ButtonCommandErrorEventArgs> ButtonCommandErrored
		{
			add => _error.Register(value);
			remove => _error.Unregister(value);
		}
		private AsyncEvent<ButtonCommandsExtension, ButtonCommandErrorEventArgs> _error;

		public ButtonCommandsExtension(ButtonCommandsConfiguration config)
		{
			Configuration = config;
            Commands = new Dictionary<string, ButtonCommand>();
			ArgumentConverters = new Dictionary<Type, IButtonArgumentConverter>
			{
				[typeof(string)] = new StringConverter(),
				[typeof(bool)] = new BoolConverter(),
				[typeof(int)] = new IntConverter(),
				[typeof(uint)] = new UintConverter(),
				[typeof(long)] = new LongConverter(),
				[typeof(ulong)] = new UlongConverter(),
				[typeof(float)] = new FloatConverter(),
				[typeof(double)] = new DoubleConverter(),
				[typeof(DiscordUser)] = new DiscordUserConverter(),
				[typeof(DiscordMember)] = new DiscordMemberConverter(),
				[typeof(DiscordRole)] = new DiscordRoleConverter(),
				[typeof(DiscordChannel)] = new DiscordChannelConverter()
			};
			ConvertMethod = typeof(ButtonCommandsExtension).GetTypeInfo().DeclaredMethods.FirstOrDefault(xm =>
				xm.Name == "ConvertArgument" && xm.ContainsGenericParameters && !xm.IsStatic);
			ConvertToButtonIdMethod = typeof(ButtonCommandsExtension).GetTypeInfo().DeclaredMethods.FirstOrDefault(xm =>
				xm.Name == "ConvertArgumentToButtonId" && xm.ContainsGenericParameters && !xm.IsStatic);
		}

        public override void Dispose()
        {
            Client.ComponentInteractionCreated -= HandleButton;
            Disposed = true;
        }

        protected override void Setup(DiscordClient client)
		{
			if (Client != null) throw new InvalidOperationException("Do NOT run Setup() yourself.");
			Client = client;
			client.ComponentInteractionCreated += HandleButton;
			_executed = new AsyncEvent<ButtonCommandsExtension, ButtonCommandExecutionEventArgs>("BUTTONCOMMAND_EXECUTED", null);
			_error = new AsyncEvent<ButtonCommandsExtension, ButtonCommandErrorEventArgs>("BUTTONCOMMAND_ERRORED", null);
		}

		private async Task HandleButton(DiscordClient sender, ComponentInteractionCreateEventArgs args)
        {
            if (!args.Id.StartsWith(Configuration.Prefix)) return;

			List<string> commandArgs = new(args.Id.Split(Configuration.ArgumentSeparator));
			string commandName = commandArgs.First()[1..];
			commandArgs.RemoveAt(0);

			if (!Commands.ContainsKey(commandName)) return;
			ButtonCommand command = Commands[commandName];

			ButtonContext ctx = BuildContext(sender, args);
			object[] arguments;
			try
			{
				arguments =
					await BuildArguments(command.Method, commandArgs, ctx);
			}
			catch (Exception e)
			{
				await _error.InvokeAsync(this, new ButtonCommandErrorEventArgs
				{
					ButtonId = args.Id,
					CommandName = commandName,
					Context = ctx,
					Exception = new Exception($"An error has occured while executing button command '{command}'.", e),
				});
				return;
			}

            ButtonCommandModule instance = (ButtonCommandModule)SpawnInstance(command);

			bool shouldRun = await instance.BeforeButtonExecutionAsync(ctx);
			if(!shouldRun) return;

			try
			{
				await (Task)command.Method.Invoke(instance, arguments);
				await instance.AfterButtonExecutionAsync(ctx);
				await _executed.InvokeAsync(this, new ButtonCommandExecutionEventArgs
				{
					ButtonId = args.Id,
					CommandName = commandName,
					Context = ctx,
				});
			}
			catch (Exception e)
			{
				await _error.InvokeAsync(this, new ButtonCommandErrorEventArgs
				{
					ButtonId = args.Id,
					CommandName = commandName,
					Context = ctx,
					Exception = new Exception($"An error has occured while executing button command '{command}'.", e),
				});
			}
		}


		private async Task<object[]> BuildArguments(MethodInfo method, List<string> args, ButtonContext ctx)
		{
			List<object> res = new List<object>
				{ ctx };

			for (var i = 1; i < method.GetParameters().Length; i++)
				res.Add(await ConvertArgument(method.GetParameters()[i], args[i - 1], ctx));

			return res.ToArray();
		}

		private ButtonContext BuildContext(DiscordClient cli, ComponentInteractionCreateEventArgs args)
		{
			return new ButtonContext
			{
				Channel = args.Channel,
				Client = cli,
				Guild = args.Guild,
				Interaction = args.Interaction,
				Message = args.Message,
				User = args.User,
				Values = args.Values,
				ButtonCommandsExtension = this
			};
		}

		private async Task<object> ConvertArgument<T>(string arg, ButtonContext ctx)
		{
			if (!ArgumentConverters.TryGetValue(typeof(T), out IButtonArgumentConverter conv))
				throw new ArgumentException($"Unknown argument type '{typeof(T).Name}'");
			if (conv is IButtonArgumentConverter<T> converter)
				return (await converter.ConvertAsync(arg, ctx)).Value;

			throw new ArgumentException($"Invalid converter registered for '{typeof(T).Name}'");
		}

		private async Task<object> ConvertArgument(ParameterInfo parameterInfo, string arg, ButtonContext ctx)
		{
			MethodInfo m = ConvertMethod.MakeGenericMethod(parameterInfo.ParameterType);
			try
			{
				return await (m.Invoke(this, new object[] { arg, ctx }) as Task<object>);
			}
			catch (ArgumentException)
			{
				throw new InvalidCastException($"An argument type of {parameterInfo.ParameterType} is not supported. Try adding a converter using ButtonCommandsExtension#AddConverter");
			}
			catch (TargetInvocationException ex)
			{
				throw ex.InnerException;
			}
		}

		private object SpawnInstance(ButtonCommand command) =>
			InstanceCreator.CreateInstance(command.Method.DeclaringType ?? typeof(ButtonCommandModule), Configuration.Services);

		public void RegisterButtons<T>() => RegisterButtons(typeof(T));

		public void RegisterButtons(Type type)
		{
            if (Disposed) throw new ObjectDisposedException(nameof(ButtonCommandsExtension));

            foreach (MethodInfo method in type.GetMethods())
			{
				ButtonCommandAttribute attr = method.GetCustomAttribute<ButtonCommandAttribute>();
				if (attr == null) continue;

				Commands.Add(attr.Name, new ButtonCommand
				{
					Method = method,
					Name = attr.Name
				});
			}
		}
		
		public void RegisterButtons(Assembly assembly)
		{
            if (Disposed) throw new ObjectDisposedException(nameof(ButtonCommandsExtension));

            foreach (TypeInfo type in assembly.DefinedTypes
                         .Where(t => t.IsPublic && t.IsAssignableTo(typeof(ButtonCommandModule))))
			{
				RegisterButtons(type);
			}
		}

		public void RegisterConverter<T>(IButtonArgumentConverter<T> converter)
		{
			if(Disposed) throw new ObjectDisposedException(nameof(ButtonCommandsExtension));

			if (converter == null)
				throw new ArgumentNullException(nameof(converter), "Converter cannot be null.");

			if (ArgumentConverters.ContainsKey(typeof(T)))
				throw new ArgumentException($"Another converter for {typeof(T).Name} has already been registered.");
			
			ArgumentConverters.Add(typeof(T), converter);
		}

		public bool UnregisterConverter<T>() => ArgumentConverters.Remove(typeof(T));

		public string BuildButtonId(string commandName, params object[] arguments)
		{
            if (Disposed) throw new ObjectDisposedException(nameof(ButtonCommandsExtension));

            StringBuilder sb = new(Configuration.Prefix + commandName);

			foreach (object arg in arguments)
				sb.Append(Configuration.ArgumentSeparator + ConvertArgumentToButtonId(arg));

			return sb.ToString();
		}

		private string ConvertArgumentToButtonId<T>(T arg)
		{
			if (!ArgumentConverters.TryGetValue(typeof(T), out IButtonArgumentConverter conv))
				throw new ArgumentException($"Unknown argument type '{typeof(T).Name}'");
			if (conv is IButtonArgumentConverter<T> converter)
				return converter.ConvertToString(arg);

			throw new ArgumentException($"Invalid converter registered for '{typeof(T).Name}'");
		}

		private string ConvertArgumentToButtonId(object arg)
		{
			MethodInfo m = ConvertToButtonIdMethod.MakeGenericMethod(arg.GetType());
			try
			{
				return m.Invoke(this, new[] { arg }) as string;
			}
			catch (ArgumentException)
			{
				throw new InvalidCastException($"An argument type of {arg.GetType().FullName} is not supported. Try adding a converter using ButtonCommandsExtension#AddConverter");
			}
			catch (TargetInvocationException ex)
			{
				throw ex.InnerException;
			}
		}
	}
}
