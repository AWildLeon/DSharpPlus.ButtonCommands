using System.Threading.Tasks;

namespace DSharpPlus.ButtonCommands
{
	public abstract class ButtonCommandModule
	{ 
		/// <summary>
		///     Called before the execution of a button command in the module.
		/// </summary>
		/// <param name="ctx">The context.</param>
		/// <returns> Whether or not to execute the button command.</returns>
		public virtual Task<bool> BeforeButtonExecutionAsync(ButtonContext ctx)
			=> Task.FromResult(true);

		/// <summary>
		///     Called after the execution of a button command in the module.
		/// </summary>
		/// <param name="ctx">The context.</param>
		public virtual Task AfterButtonExecutionAsync(ButtonContext ctx)
			=> Task.CompletedTask;
	}
}
