using DSharpPlus.Entities;

namespace DSharpPlus.ButtonCommands
{
	public class ButtonContext
	{
		public DiscordInteraction Interaction { get; set; }
		public DiscordClient Client { get; set; }
		public DiscordGuild Guild { get; set; }
		public DiscordChannel Channel { get; set; }
		public DiscordMessage Message { get; set; }
		public DiscordUser User { get; set; }
		public DiscordMember Member
			=> User is DiscordMember member ? member : null;
		public ButtonCommandsExtension ButtonCommandsExtension { get; set; }
		public string[] Values { get; set; }
	}
}